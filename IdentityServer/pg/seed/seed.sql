-- -------------------------------------------------------------
-- TablePlus 2.2(212)
--
-- https://tableplus.com/
--
-- Database: IdentityServer
-- Generation Time: 2019-04-14 18:50:35.7240
-- -------------------------------------------------------------


-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."__EFMigrationsHistory" (
    "MigrationId" varchar(150) NOT NULL,
    "ProductVersion" varchar(32) NOT NULL,
    PRIMARY KEY ("MigrationId")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ApiClaims_Id_seq";

-- Table Definition
CREATE TABLE "public"."ApiClaims" (
    "Id" int4 NOT NULL DEFAULT nextval('"ApiClaims_Id_seq"'::regclass),
    "Type" varchar(200) NOT NULL,
    "ApiResourceId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ApiProperties_Id_seq";

-- Table Definition
CREATE TABLE "public"."ApiProperties" (
    "Id" int4 NOT NULL DEFAULT nextval('"ApiProperties_Id_seq"'::regclass),
    "Key" varchar(250) NOT NULL,
    "Value" varchar(2000) NOT NULL,
    "ApiResourceId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ApiResources_Id_seq";

-- Table Definition
CREATE TABLE "public"."ApiResources" (
    "Id" int4 NOT NULL DEFAULT nextval('"ApiResources_Id_seq"'::regclass),
    "Enabled" bool NOT NULL,
    "Name" varchar(200) NOT NULL,
    "DisplayName" varchar(200),
    "Description" varchar(1000),
    "Created" timestamp NOT NULL,
    "Updated" timestamp,
    "LastAccessed" timestamp,
    "NonEditable" bool NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ApiScopeClaims_Id_seq";

-- Table Definition
CREATE TABLE "public"."ApiScopeClaims" (
    "Id" int4 NOT NULL DEFAULT nextval('"ApiScopeClaims_Id_seq"'::regclass),
    "Type" varchar(200) NOT NULL,
    "ApiScopeId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ApiScopes_Id_seq";

-- Table Definition
CREATE TABLE "public"."ApiScopes" (
    "Id" int4 NOT NULL DEFAULT nextval('"ApiScopes_Id_seq"'::regclass),
    "Name" varchar(200) NOT NULL,
    "DisplayName" varchar(200),
    "Description" varchar(1000),
    "Required" bool NOT NULL,
    "Emphasize" bool NOT NULL,
    "ShowInDiscoveryDocument" bool NOT NULL,
    "ApiResourceId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ApiSecrets_Id_seq";

-- Table Definition
CREATE TABLE "public"."ApiSecrets" (
    "Id" int4 NOT NULL DEFAULT nextval('"ApiSecrets_Id_seq"'::regclass),
    "Description" varchar(1000),
    "Value" varchar(4000) NOT NULL,
    "Expiration" timestamp,
    "Type" varchar(250) NOT NULL,
    "Created" timestamp NOT NULL,
    "ApiResourceId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientClaims_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientClaims" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientClaims_Id_seq"'::regclass),
    "Type" varchar(250) NOT NULL,
    "Value" varchar(250) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientCorsOrigins_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientCorsOrigins" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientCorsOrigins_Id_seq"'::regclass),
    "Origin" varchar(150) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientGrantTypes_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientGrantTypes" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientGrantTypes_Id_seq"'::regclass),
    "GrantType" varchar(250) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientIdPRestrictions_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientIdPRestrictions" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientIdPRestrictions_Id_seq"'::regclass),
    "Provider" varchar(200) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientPostLogoutRedirectUris_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientPostLogoutRedirectUris" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientPostLogoutRedirectUris_Id_seq"'::regclass),
    "PostLogoutRedirectUri" varchar(2000) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientProperties_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientProperties" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientProperties_Id_seq"'::regclass),
    "Key" varchar(250) NOT NULL,
    "Value" varchar(2000) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientRedirectUris_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientRedirectUris" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientRedirectUris_Id_seq"'::regclass),
    "RedirectUri" varchar(2000) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "Clients_Id_seq";

-- Table Definition
CREATE TABLE "public"."Clients" (
    "Id" int4 NOT NULL DEFAULT nextval('"Clients_Id_seq"'::regclass),
    "Enabled" bool NOT NULL,
    "ClientId" varchar(200) NOT NULL,
    "ProtocolType" varchar(200) NOT NULL,
    "RequireClientSecret" bool NOT NULL,
    "ClientName" varchar(200),
    "Description" varchar(1000),
    "ClientUri" varchar(2000),
    "LogoUri" varchar(2000),
    "RequireConsent" bool NOT NULL,
    "AllowRememberConsent" bool NOT NULL,
    "AlwaysIncludeUserClaimsInIdToken" bool NOT NULL,
    "RequirePkce" bool NOT NULL,
    "AllowPlainTextPkce" bool NOT NULL,
    "AllowAccessTokensViaBrowser" bool NOT NULL,
    "FrontChannelLogoutUri" varchar(2000),
    "FrontChannelLogoutSessionRequired" bool NOT NULL,
    "BackChannelLogoutUri" varchar(2000),
    "BackChannelLogoutSessionRequired" bool NOT NULL,
    "AllowOfflineAccess" bool NOT NULL,
    "IdentityTokenLifetime" int4 NOT NULL,
    "AccessTokenLifetime" int4 NOT NULL,
    "AuthorizationCodeLifetime" int4 NOT NULL,
    "ConsentLifetime" int4,
    "AbsoluteRefreshTokenLifetime" int4 NOT NULL,
    "SlidingRefreshTokenLifetime" int4 NOT NULL,
    "RefreshTokenUsage" int4 NOT NULL,
    "UpdateAccessTokenClaimsOnRefresh" bool NOT NULL,
    "RefreshTokenExpiration" int4 NOT NULL,
    "AccessTokenType" int4 NOT NULL,
    "EnableLocalLogin" bool NOT NULL,
    "IncludeJwtId" bool NOT NULL,
    "AlwaysSendClientClaims" bool NOT NULL,
    "ClientClaimsPrefix" varchar(200),
    "PairWiseSubjectSalt" varchar(200),
    "Created" timestamp NOT NULL,
    "Updated" timestamp,
    "LastAccessed" timestamp,
    "UserSsoLifetime" int4,
    "UserCodeType" varchar(100),
    "DeviceCodeLifetime" int4 NOT NULL,
    "NonEditable" bool NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientScopes_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientScopes" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientScopes_Id_seq"'::regclass),
    "Scope" varchar(200) NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "ClientSecrets_Id_seq";

-- Table Definition
CREATE TABLE "public"."ClientSecrets" (
    "Id" int4 NOT NULL DEFAULT nextval('"ClientSecrets_Id_seq"'::regclass),
    "Description" varchar(2000),
    "Value" varchar(4000) NOT NULL,
    "Expiration" timestamp,
    "Type" varchar(250) NOT NULL,
    "Created" timestamp NOT NULL,
    "ClientId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."DeviceCodes" (
    "UserCode" varchar(200) NOT NULL,
    "DeviceCode" varchar(200) NOT NULL,
    "SubjectId" varchar(200),
    "ClientId" varchar(200) NOT NULL,
    "CreationTime" timestamp NOT NULL,
    "Expiration" timestamp NOT NULL,
    "Data" varchar(50000) NOT NULL,
    PRIMARY KEY ("UserCode")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "IdentityClaims_Id_seq";

-- Table Definition
CREATE TABLE "public"."IdentityClaims" (
    "Id" int4 NOT NULL DEFAULT nextval('"IdentityClaims_Id_seq"'::regclass),
    "Type" varchar(200) NOT NULL,
    "IdentityResourceId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "IdentityProperties_Id_seq";

-- Table Definition
CREATE TABLE "public"."IdentityProperties" (
    "Id" int4 NOT NULL DEFAULT nextval('"IdentityProperties_Id_seq"'::regclass),
    "Key" varchar(250) NOT NULL,
    "Value" varchar(2000) NOT NULL,
    "IdentityResourceId" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "IdentityResources_Id_seq";

-- Table Definition
CREATE TABLE "public"."IdentityResources" (
    "Id" int4 NOT NULL DEFAULT nextval('"IdentityResources_Id_seq"'::regclass),
    "Enabled" bool NOT NULL,
    "Name" varchar(200) NOT NULL,
    "DisplayName" varchar(200),
    "Description" varchar(1000),
    "Required" bool NOT NULL,
    "Emphasize" bool NOT NULL,
    "ShowInDiscoveryDocument" bool NOT NULL,
    "Created" timestamp NOT NULL,
    "Updated" timestamp,
    "NonEditable" bool NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."PersistedGrants" (
    "Key" varchar(200) NOT NULL,
    "Type" varchar(50) NOT NULL,
    "SubjectId" varchar(200),
    "ClientId" varchar(200) NOT NULL,
    "CreationTime" timestamp NOT NULL,
    "Expiration" timestamp,
    "Data" varchar(50000) NOT NULL,
    PRIMARY KEY ("Key")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "RoleClaims_Id_seq";

-- Table Definition
CREATE TABLE "public"."RoleClaims" (
    "Id" int4 NOT NULL DEFAULT nextval('"RoleClaims_Id_seq"'::regclass),
    "RoleId" text NOT NULL,
    "ClaimType" text,
    "ClaimValue" text,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."Roles" (
    "Id" text NOT NULL,
    "Name" varchar(256),
    "NormalizedName" varchar(256),
    "ConcurrencyStamp" text,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "UserClaims_Id_seq";

-- Table Definition
CREATE TABLE "public"."UserClaims" (
    "Id" int4 NOT NULL DEFAULT nextval('"UserClaims_Id_seq"'::regclass),
    "UserId" text NOT NULL,
    "ClaimType" text,
    "ClaimValue" text,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."UserLogins" (
    "LoginProvider" text NOT NULL,
    "ProviderKey" text NOT NULL,
    "ProviderDisplayName" text,
    "UserId" text NOT NULL,
    PRIMARY KEY ("LoginProvider","ProviderKey")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."UserRoles" (
    "UserId" text NOT NULL,
    "RoleId" text NOT NULL,
    PRIMARY KEY ("UserId","RoleId")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."Users" (
    "Id" text NOT NULL,
    "UserName" varchar(256),
    "NormalizedUserName" varchar(256),
    "Email" varchar(256),
    "NormalizedEmail" varchar(256),
    "EmailConfirmed" bool NOT NULL,
    "PasswordHash" text,
    "SecurityStamp" text,
    "ConcurrencyStamp" text,
    "PhoneNumber" text,
    "PhoneNumberConfirmed" bool NOT NULL,
    "TwoFactorEnabled" bool NOT NULL,
    "LockoutEnd" timestamptz,
    "LockoutEnabled" bool NOT NULL,
    "AccessFailedCount" int4 NOT NULL,
    PRIMARY KEY ("Id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."UserTokens" (
    "UserId" text NOT NULL,
    "LoginProvider" text NOT NULL,
    "Name" text NOT NULL,
    "Value" text,
    PRIMARY KEY ("UserId","LoginProvider","Name")
);

INSERT INTO "public"."__EFMigrationsHistory" ("MigrationId", "ProductVersion") VALUES ('20190413182053_InitialIdentityServerPersistedGrantDbMigration', '2.2.3-servicing-35854'),
('20190413182114_InitialIdentityServerConfigurationDbMigration', '2.2.3-servicing-35854'),
('20190413182129_InitialIdentityServerIdentityDbMigration', '2.2.3-servicing-35854');

INSERT INTO "public"."ApiResources" ("Id", "Enabled", "Name", "DisplayName", "Description", "Created", "Updated", "LastAccessed", "NonEditable") VALUES ('1', 't', 'api', 'api', NULL, '2019-04-04 17:40:05.210054', NULL, NULL, 'f');

INSERT INTO "public"."ApiScopes" ("Id", "Name", "DisplayName", "Description", "Required", "Emphasize", "ShowInDiscoveryDocument", "ApiResourceId") VALUES ('1', 'api', 'api', NULL, 'f', 'f', 't', '1');

INSERT INTO "public"."ClientCorsOrigins" ("Id", "Origin", "ClientId") VALUES ('1', 'http://localhost:5003', '1'),
('2', 'https://identityserver-spa.azurewebsites.net', '1');

INSERT INTO "public"."ClientGrantTypes" ("Id", "GrantType", "ClientId") VALUES ('1', 'implicit', '2'),
('2', 'implicit', '1');

INSERT INTO "public"."ClientPostLogoutRedirectUris" ("Id", "PostLogoutRedirectUri", "ClientId") VALUES ('1', 'http://localhost:9000', '2'),
('2', 'http://localhost:5003/index.html', '1');

INSERT INTO "public"."ClientRedirectUris" ("Id", "RedirectUri", "ClientId") VALUES ('1', 'http://localhost:9000/signin-oidc', '2'),
('2', 'http://localhost:5003/silent-renew.html', '1'),
('3', 'http://localhost:5003/callback.html', '1');

INSERT INTO "public"."Clients" ("Id", "Enabled", "ClientId", "ProtocolType", "RequireClientSecret", "ClientName", "Description", "ClientUri", "LogoUri", "RequireConsent", "AllowRememberConsent", "AlwaysIncludeUserClaimsInIdToken", "RequirePkce", "AllowPlainTextPkce", "AllowAccessTokensViaBrowser", "FrontChannelLogoutUri", "FrontChannelLogoutSessionRequired", "BackChannelLogoutUri", "BackChannelLogoutSessionRequired", "AllowOfflineAccess", "IdentityTokenLifetime", "AccessTokenLifetime", "AuthorizationCodeLifetime", "ConsentLifetime", "AbsoluteRefreshTokenLifetime", "SlidingRefreshTokenLifetime", "RefreshTokenUsage", "UpdateAccessTokenClaimsOnRefresh", "RefreshTokenExpiration", "AccessTokenType", "EnableLocalLogin", "IncludeJwtId", "AlwaysSendClientClaims", "ClientClaimsPrefix", "PairWiseSubjectSalt", "Created", "Updated", "LastAccessed", "UserSsoLifetime", "UserCodeType", "DeviceCodeLifetime", "NonEditable") VALUES ('1', 't', 'js', 'oidc', 'f', 'JavaScript Client', NULL, NULL, NULL, 't', 't', 'f', 'f', 'f', 't', NULL, 't', NULL, 't', 'f', '300', '120', '300', NULL, '2592000', '1296000', '1', 'f', '1', '0', 't', 'f', 'f', 'client_', NULL, '2019-04-13 21:52:59.407497', NULL, NULL, NULL, NULL, '300', 'f'),
('2', 't', 'admin', 'oidc', 't', 'Admin Client', NULL, NULL, NULL, 't', 't', 'f', 'f', 'f', 't', NULL, 't', NULL, 't', 'f', '300', '1296000', '300', NULL, '2592000', '1296000', '1', 'f', '1', '0', 't', 'f', 'f', 'client_', NULL, '2019-04-13 21:52:27.348521', NULL, NULL, NULL, NULL, '300', 'f');

INSERT INTO "public"."ClientScopes" ("Id", "Scope", "ClientId") VALUES ('1', 'email', '2'),
('2', 'roles', '2'),
('3', 'profile', '2'),
('4', 'openid', '2'),
('5', 'openid', '1'),
('6', 'profile', '1'),
('7', 'api', '1');

INSERT INTO "public"."ClientSecrets" ("Id", "Description", "Value", "Expiration", "Type", "Created", "ClientId") VALUES ('1', NULL, 'vSsar3708Jvp9Szi2NWZZ02Bqp1qRCFpbcTZPdBhnWgs5WtNZKnvCXdhztmeD2cmW192CF5bDufKRpayrW/isg==', NULL, 'SharedSecret', '2019-04-10 09:08:02.291195', '2');

INSERT INTO "public"."IdentityClaims" ("Id", "Type", "IdentityResourceId") VALUES ('1', 'sub', '1'),
('2', 'name', '2'),
('3', 'family_name', '2'),
('4', 'given_name', '2'),
('5', 'middle_name', '2'),
('6', 'nickname', '2'),
('7', 'preferred_username', '2'),
('8', 'profile', '2'),
('9', 'picture', '2'),
('10', 'website', '2'),
('11', 'gender', '2'),
('12', 'birthdate', '2'),
('13', 'zoneinfo', '2'),
('14', 'locale', '2'),
('15', 'updated_at', '2'),
('16', 'role', '5');

INSERT INTO "public"."IdentityResources" ("Id", "Enabled", "Name", "DisplayName", "Description", "Required", "Emphasize", "ShowInDiscoveryDocument", "Created", "Updated", "NonEditable") VALUES ('1', 't', 'openid', 'Your user identifier', NULL, 't', 'f', 't', '2019-04-04 17:40:04.318685', NULL, 'f'),
('2', 't', 'profile', 'User profile', 'Your user profile information (first name, last name, etc.)', 'f', 't', 't', '2019-04-04 17:40:04.351973', NULL, 'f'),
('4', 't', 'email', 'Email', 'Email', 'f', 't', 't', '2019-04-10 00:00:00', NULL, 'f'),
('5', 't', 'roles', 'Roles', 'Roles', 'f', 't', 't', '2019-04-10 00:00:00', NULL, 'f');

INSERT INTO "public"."PersistedGrants" ("Key", "Type", "SubjectId", "ClientId", "CreationTime", "Expiration", "Data") VALUES ('W81YY+NX1kAvcYxD5f52RJ3qRkcLa3Jy06x+26eBFCI=', 'user_consent', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'js', '2019-04-08 10:33:10', NULL, '{"SubjectId":"de234e25-d306-4baf-b35f-e47f5af5cdd9","ClientId":"js","Scopes":["openid","profile","api"],"CreationTime":"2019-04-08T10:33:10Z","Expiration":null}'),
('tf5AScLfBc+pxVn6lzw8NZs/3jk1q+70oUs4CDN1skM=', 'user_consent', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'admin', '2019-04-10 08:56:54', NULL, '{"SubjectId":"de234e25-d306-4baf-b35f-e47f5af5cdd9","ClientId":"admin","Scopes":["openid","profile","email","roles"],"CreationTime":"2019-04-10T08:56:54Z","Expiration":null}');

INSERT INTO "public"."Roles" ("Id", "Name", "NormalizedName", "ConcurrencyStamp") VALUES ('1', 'admin', 'Admin', NULL);

INSERT INTO "public"."UserClaims" ("Id", "UserId", "ClaimType", "ClaimValue") VALUES ('1', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'name', 'Alice Smith'),
('2', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'given_name', 'Alice'),
('3', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'family_name', 'Smith'),
('4', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'email', 'AliceSmith@email.com'),
('5', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'email_verified', 'true'),
('6', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'website', 'http://alice.com'),
('7', 'de234e25-d306-4baf-b35f-e47f5af5cdd9', 'address', '{ ''street_address'': ''One Hacker Way'', ''locality'': ''Heidelberg'', ''postal_code'': 69118, ''country'': ''Germany'' }'),
('8', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'name', 'Bob Smith'),
('9', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'given_name', 'Bob'),
('10', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'family_name', 'Smith'),
('11', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'email', 'BobSmith@email.com'),
('12', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'email_verified', 'true'),
('13', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'website', 'http://bob.com'),
('14', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'address', '{ ''street_address'': ''One Hacker Way'', ''locality'': ''Heidelberg'', ''postal_code'': 69118, ''country'': ''Germany'' }'),
('15', 'be8108fe-57a4-48eb-9270-6f17edf51210', 'location', 'somewhere');

INSERT INTO "public"."UserRoles" ("UserId", "RoleId") VALUES ('de234e25-d306-4baf-b35f-e47f5af5cdd9', '1');

INSERT INTO "public"."Users" ("Id", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount") VALUES ('be8108fe-57a4-48eb-9270-6f17edf51210', 'bob', 'BOB', NULL, NULL, 'f', 'AQAAAAEAACcQAAAAEJKC7jGx+cLRLSjV4rjeBTQLMiOca7D6llEYAgpjEm05x7g9iKlO3tNgVJs5iTf3IA==', '2KWYAIBM3CBTD2BFFDE6ELFH4UMVSANE', 'df9bc4fb-2349-494c-a1c8-c24ab917fa3e', NULL, 'f', 'f', NULL, 't', '0'),
('de234e25-d306-4baf-b35f-e47f5af5cdd9', 'alice', 'ALICE', NULL, NULL, 'f', 'AQAAAAEAACcQAAAAEPiKnMeeqnQAjS7EDp2r1aT/9Ox1qqYLI5Z9RZbGZgXPMeTweWr6H21e20fNRrjC5g==', 'A5PIJOUXZQXB3QREM4KUHVTB6MP5LEWC', '67382ce7-8db5-4098-8d28-2b3253d96a1b', NULL, 'f', 'f', NULL, 't', '0');

ALTER TABLE "public"."ApiClaims" ADD FOREIGN KEY ("ApiResourceId") REFERENCES "public"."ApiResources"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ApiProperties" ADD FOREIGN KEY ("ApiResourceId") REFERENCES "public"."ApiResources"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ApiScopeClaims" ADD FOREIGN KEY ("ApiScopeId") REFERENCES "public"."ApiScopes"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ApiScopes" ADD FOREIGN KEY ("ApiResourceId") REFERENCES "public"."ApiResources"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ApiSecrets" ADD FOREIGN KEY ("ApiResourceId") REFERENCES "public"."ApiResources"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientClaims" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientCorsOrigins" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientGrantTypes" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientIdPRestrictions" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientPostLogoutRedirectUris" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientProperties" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientRedirectUris" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientScopes" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."ClientSecrets" ADD FOREIGN KEY ("ClientId") REFERENCES "public"."Clients"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."IdentityClaims" ADD FOREIGN KEY ("IdentityResourceId") REFERENCES "public"."IdentityResources"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."IdentityProperties" ADD FOREIGN KEY ("IdentityResourceId") REFERENCES "public"."IdentityResources"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."RoleClaims" ADD FOREIGN KEY ("RoleId") REFERENCES "public"."Roles"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."UserClaims" ADD FOREIGN KEY ("UserId") REFERENCES "public"."Users"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."UserLogins" ADD FOREIGN KEY ("UserId") REFERENCES "public"."Users"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."UserRoles" ADD FOREIGN KEY ("RoleId") REFERENCES "public"."Roles"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."UserRoles" ADD FOREIGN KEY ("UserId") REFERENCES "public"."Users"("Id") ON DELETE CASCADE;
ALTER TABLE "public"."UserTokens" ADD FOREIGN KEY ("UserId") REFERENCES "public"."Users"("Id") ON DELETE CASCADE;
